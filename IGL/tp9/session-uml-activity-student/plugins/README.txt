/*--------------------------------------------------------------------------------------------------------------*/
 * The jar contains a model library that is required
 * to be deployed on your local eclipse installation
 * in order to realize the fist lab. Guideline to achieve
 * this task are explained below.
 * 
 * If you have the write permission on the Eclipse installation
 * folder.  
 *
 * [1] Open you the directory containing the eclipe installation
 * [2] Create a "dropins" directory
 * [3] Copy the jar to the "dropins" directory
 * [4] Restart you Eclipse if it was opened
 * 
 * If you do not have the write permission
 * 
 * [1] Open Eclipse
 * [2] Import existing project com.cea.moka.fuml.maths.zip (File -> Import -> Existing project into workspace)
 * [3] Create a new runtime configuration for an Eclipse application (Run -> Run configurations)
 * [4] Do all your exercises in this new Eclipse instance.   
/*--------------------------------------------------------------------------------------------------------------*/