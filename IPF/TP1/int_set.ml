exception EmptySet;;

type int_set =  int list;;

let is_empty s = match s with
	| [] -> true 
	| _  -> false ;;

let empty = [] ;; 

let rec mem e s =  match s with 
	| a::r -> if a = e then true else mem e r 
    | [] -> false ;; 

let rec add e s = match s with 
	| a::r -> if e > a then a::(add e r) else if a = e then e::r else e::a::r
	| []   -> [e] ;; 

let rec fold f s v0  = match s with
	| a::r -> fold f r (f a v0)
	| []   -> v0;;

let rec get_min s = if is_empty s then raise EmptySet else List.hd s ;;
(**;; if a < (get_min r) then a else get_min r ;;**)

let rec equal s t = match (s,t) with
	| (x1::r1, x2::r2) -> if x1 = x2 then equal r1 r2 else false
	| ([], _)          -> false
	| (_, [])          -> false;;
	

let rec remove e s = match s with 
	| a::r -> if a = e then (remove e r) else a::(remove e r)
	| []   -> [];;

let rec union s t = match (s,t) with 
	| ([], _) 		   -> t
	| (_, []) 		   -> s
	| (x1::r1, x2::r2) -> if x1 <= x2 then x1::(union r1 t)
									  else x2::(union r2 s);;