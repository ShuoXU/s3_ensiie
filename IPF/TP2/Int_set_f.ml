exception EmptySet

            
module type IntSet =
  sig
    type int_set
    val is_empty : int_set -> bool
    val empty : int_set
    val mem : int -> int_set -> bool
    val add : int -> int_set -> int_set
    val fold : (int -> 'a -> 'a) -> int_set -> 'a -> 'a
    val get_min : int_set -> int
    val equal : int_set -> int_set ->bool
    val remove : int -> int_set -> int_set
    val union : int_set -> int_set -> int_set
  end 



            
module IntSetList = 
         
  struct
    
    type int_set =  int list

    let is_empty s = match s with
      | [] -> true 
      | _  -> false 

    let empty = []    
      
    let rec mem e s =  match s with 
      | a::r -> if a = e then true else mem e r 
      | [] -> false    
      
    let rec add e s = match s with 
      | a::r -> if e > a then a::(add e r) else if a = e then e::r else e::a::r
      | []   -> [e]    
      
    let rec fold f s v0  = match s with
      | a::r -> fold f r (f a v0)
      | []   -> v0
      
    let rec get_min s = if is_empty s then raise EmptySet else List.hd s
                                                                       
    let rec equal s t = match (s,t) with
      | (x1::r1, x2::r2) -> if x1 = x2 then equal r1 r2 else false
      | ([], _)          -> false
      | (_, [])          -> false
      

    let rec remove e s = match s with
      | a::r -> if a = e then (remove e r) else a::(remove e r)
      | []   -> [];;
      
    let rec union s t = match (s,t) with 
      | ([], _) 		   -> t
      | (_, []) 		   -> s
      | (x1::r1, x2::r2) -> if x1 <= x2 then x1::(union r1 t)
			    else x2::(union r2 s)
                                       
  end


            
module IntSetIntervals = 
         
  struct
    type int_set = ( int*int ) list

    let is_empty s = match s with
      | [] -> true 
      | _  -> false 

    let empty = []    
      
    let rec mem e s = match e with 
      | (a,b)::r -> if e < a then false
                    else if (a = e | b = e) then true
                    else mem e r
      | [] -> false    
      
    let rec add e s = match s with 
      | (a,b)::r -> if e <= a then (e,e)::(a,b)::r
                    else if e = b then (a,b)::(e,e)::r
                    else (a,b)::(add e r)
      | []   -> [e]    
      
    let rec fold f s v0  = match s with
      | a::r -> fold f r (f a v0)
      | []   -> v0
      
    let rec get_min s = let ((a,b) = List.hd s) in if is_empty s then raise EmptySet else a
                                                                       
    let rec equal s t = s = t(*match (s,t) with
      | ((x1,y1)::r1, (x2,y2)::r2) -> if (x1 = x2 && y1 = y2) then equal r1 r2 else false
      | ([], _)          -> false
      | (_, [])          -> false*)
      

    let rec remove e s = match s with
      | (a,b)::r -> if (a = e | b = e) then (remove e r) else a::(remove e r)
      | []   -> []
      
    let rec union s t = match (s,t) with 
      | ([], _) 		   -> t
      | (_, []) 		   -> s
      | (x1::r1, x2::r2) -> if x1 <= x2 then x1::(union r1 t)
			    else x2::(union r2 s)


  end



module IntSetAbr =

  struct


  end
    
