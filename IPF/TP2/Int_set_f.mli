(** Ensembles d'entiers

Ce module dÃ©fini une structure d'ensembles d'entiers. 

 *)


exception EmptySet
(** exception levÃ©e si l'on tente de deconstruire un ensemble vide *)
module type IntSet =
  sig
    type int_set
    val is_empty : int_set -> bool
    val empty : int_set
    val mem : int -> int_set -> bool
    val add : int -> int_set -> int_set
    val fold : (int -> 'a -> 'a) -> int_set -> 'a -> 'a
    val get_min : int_set -> int
    val equal : int_set -> int_set ->bool
    val remove : int -> int_set -> int_set
    val union : int_set -> int_set -> int_set
  end 


module IntSetList : IntSet
module IntSetAbr : IntSet
module IntSetIntervals : IntSet

