module type Ordered =
  sig
    type t
    val compare : t -> t -> int
  end

module type Ens =
  sig
    type elt
    type set
    val is_empty : set -> bool
    val empty : set
    val mem : elt -> set -> bool
    val add : elt -> set -> set
    val get_min : set -> elt
  end 


module MakeList(X:Ordered):Ens  =                    
  struct
                                                   
    type set = X.t list
 type elt = X.t
   
                 
    let empty = []
                  
    let is_empty s = s = []
                           
    let get_min s = match s with
      | [] -> raise Not_found
      | h::_ -> h
                  
    let rec add e s = match s with
      | [] -> [e]
      | h::s' -> let c = X.compare e h in
                 if c < 0 then e::s
                 else if c = 0 then s else h::add e s'
                                            
    let rec mem e s = match s with
      | [] -> false
      | h::s' -> let c = X.compare e h in
                 if c < 0 then false else if c = 0 then true else mem e s'
  end
          
        
module MakeAbr (X:Ordered):Ens =
  struct
    type elt = X.t
    type set = Empty | Node of set * elt * set
    let empty = Empty
    let is_empty s = s = Empty
    let rec mem e s = match s with
      | Empty -> false
      | Node(g,r,d) -> let c = X.compare e r in
                       if c = 0 then true else if c < 0 then mem e g else mem e d

    let rec add e s = match s with       
      | Empty -> Node(empty, e, empty)
      | Node(g,r,d) -> let c = X.compare e r in
                       if c = 0 then s else if c < 0 then Node(add e g, r, d)
                       else Node(g, r, add e d)

    let get_min s =  match s with
      | Empty -> raise Not_found
      | Node(g,r,d) -> g
  end

    
                                             
module MakeAVL (X:Ordered):Ens =
  struct
    type elt = X.t
    type set = Empty | Node of set * elt * set * int

    let node g r d = Node(g , r , d , 1 + max (height g) (height d))

    let rec mem e s = match s with
      | Empty -> false
      | Node(g,r,d,_) ->  let c = X.compare e r in
                          if c = 0 then true else if c < 0 then mem e g else mem e d
    let rec add e s = match s with
      | Empty -> node empty e empty
      | Node(g,r,d,_) -> failwith "TODO"

    let get_min s = match s with
      | Empty -> raise Not_found
      | Node(g,r,d,_) -> 
  end
    
