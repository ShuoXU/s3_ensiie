open Ensemble
       
module Entier =
  struct
    type t = int
    let compare x y = x - y
    let from_int 
  end


module EnsListeEntier = MakeList(Entier)
module EnsListeString = MakeList(struct type t = string let compare = Pervasives.compare end)

module EnsListeEnsEntier = MakeList(struct type t = EnsListeEntier.set let compare = compare end)

let test_empty = EnsListeEntier.is_empty EnsListeEntier.empty
let test_add   = EnsListeEntier.mem 42 EnsListeEntier.empty
let test_add_2 = EnsListeEntier.mem 42 (EnsListeEntier.add 56 (EnsListeEntier.add 42 (EnsListeEntier.add 76 EnsListeEntier.empty)))
