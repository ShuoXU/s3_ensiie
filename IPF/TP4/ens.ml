(*
TP 4 
Partie 2
Version 1.0
Date : 16 oct 2018

- Il reste encore deux fonctions de remove et fold à faire.
*)

module type S =
  sig
    type key
    type 'a pmap
    val empty : 'a pmap
    val is_empty : 'a pmap -> bool
    val add : key -> 'a -> 'a pmap -> 'a pmap
    val find : key -> 'a pmap -> 'a
    val remove : key -> 'a pmap -> 'a pmap
    val fold : (key -> 'a -> 'b) -> 'a pmap -> 'b -> 'b
  end

module type Ordered =
  sig
    type t
    val compare : t -> t -> int
  end

module AVLMap(X:Ordered) : S  =
  struct
    type key = X.t
    type 'a pmap =
      | Empty
      | Node of ('a pmap * X.t * 'a * 'a pmap * int)
    let empty = Empty
    let is_empty s = Empty = s
    let rec mem k m = match m with
      | Empty -> false
      | Node(g, k', _, d, _) -> let c = X.compare k k' in
                                if c = 0 then true
                                else if c < 0 then mem k g else mem k d
    let rec find k m = match m with
      | Empty -> raise Not_found
      | Node(g,k',v',d,_) ->  let c = X.compare k k' in
                                if c = 0 then v'
                                else if c < 0 then find k g else find k d
    type 'a rotation =
      | None
      | Some of 'a
    let height m = match m with
      | Empty -> 0
      | Node(_,_,_,_,h) -> h
    let node g k v d = Node(g,k,v,d,1+max (height g) (height d) )
    let  rec find_opt k m = match m with
      | Empty -> None
      | Node(g,k',v',d,_) -> let c = X.compare k k' in
                             if c = 0 then Some v'
                             else if c < 0 then find_opt k g else find_opt k d
    let balance g k v d =
      let hg = height g in
      let hd = height d in
      if hg > hd + 1 then
        match g with
        | Empty -> assert false
        | Node(gg,kg,vg,gd,_) ->
           let hgg = height gg in
           let hgd = height gd in
           if hgg > hgd then node gg kg vg (node gd k v d)
           else match gd with
                | Empty ->  assert false
                | Node(gdg,kgd,vgd,gdd,_) -> node (node gg kg vg gdg) kgd vgd (node gdd k v d)
      else if hd > hg + 1 then failwith "Blablabla" else node g k v d
                                                              
    let rec add k v m = match m with
      | Empty -> node empty k v empty
      | Node(g,k',v',d,_) -> let c = X.compare k k' in
                             if c = 0 then
                               node g k v d
                             else if c < 0 then
                               balance (add k v g) k' v' d
                             else
                               balance g k' v' (add k v d)

    let rec remove k m = match m with
      | Empty -> failwith "C'est vide."
      | Node(g,k',v',d,_) -> let c = X.compare k k' in 
                             if c = 0 then
                               node g k' v' d
                             else if c < 0 then
                               balance (remove k g) k' v' d
                             else
                               balance g k' v' (remove k d)
                                       
    let rec fold f m v0 = match m with
      | Empty -> v0
      | Node(g,k',v',d,_) -> fold f d (f k' v' (fold f g v0))
  end
    
                            
