module type S =
  sig
    type key
    type 'a pmap
    val empty : 'a pmap
    val is_empty : 'a pmap -> bool
    val add : key -> 'a -> 'a pmap -> 'a pmap
    val find : key -> 'a pmap -> 'a
    val remove : key -> 'a pmap -> 'a pmap
    val fold : (key -> 'a -> 'b) -> 'a pmap -> 'b -> 'b
  end

module type Ordered =
  sig
    type t
    val compare : t -> t -> int
  end

module AVLMap(X:Ordered) : S 
