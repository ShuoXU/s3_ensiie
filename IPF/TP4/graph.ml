module type Graph =
  sig
    type node
    module NodeSet : Set.S with type elt = node
    type graph
    val empty : graph
    val is_empty : graph -> bool
    val add_vertex : node -> graph -> graph
    val add_edge : node -> node -> graph -> graph
    val succs : node -> graph -> NodeSet.t
    val fold_node : (node -> 'a -> 'a) -> graph -> 'a -> 'a
    val fold_edge : (node -> node -> 'a -> 'a) -> graph -> 'a ->'a
  end


module MakeGraph(X:Set.OrderedType) =
  struct
    type node = X.t
    module NodeSet = Set.Make(X)
    module NodeMap = Ens.AVLMap(X)
    type graph = NodeSet.t * NodeSet.t
    let empty = NodeMap.empty
    let is_empty g = NodeMap.is_empty g
    let add_vertex v g = if NodeMap.mem v g then g
                         else NodeMap.add v NodeSet.empty g
    let succs v g = NodeMap.find v g
    let add_edge v1 v2 g = let g1 = add_vertex v1 g in
                           let g2 = add_vertex v2 g1 in
                           let v1_succs = succs v1 g2 in
                           let v1_succs' = NodeSet.add v2 v1_succs in
                           NodeMap.add v1 v1_succ' g2
    let fold_node f g v0 = NodeMap.fold (fun v v_succs acc -> f v acc) g v0
    let fold_edge f g v0 = NodeMap.fold (fun v v_succs acc -> NodeSet.fold (fun v' acc -> f v v' acc) v_succs acc) g v0
  end
    

module type Ext =
  sig
    type node
    type graph
    val recherche_chemin : node -> node -> graph -> node list
  end


module GraphExt(G:Graph) =
  struct
    type node = G.node
    type graph = G.graph
    let recherche_chemin v1 v2 g = failwith "todo" 
  end


