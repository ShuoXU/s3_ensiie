module type Graph =
  sig
    type node
    module NodeSet : Set.S with type elt = node
    type graph
    val empty : graph
    val is_empty : graph -> bool
    val add_vertex : node -> graph -> graph
    val add_edge : node -> node -> graph -> graph
    val succs : node -> graph -> NodeSet.t
    val fold_node : (node -> 'a -> 'a) -> graph -> 'a -> 'a
    val fold_edge : (node -> node -> 'a -> 'a) -> graph -> 'a ->'a
  end


module MakeGraph(X:Set.OrderedType) : Graph with type node = X.t

module type Ext =
  sig
    type node
    type graph
    val recherche_chemin : node -> node -> graph -> node list
  end
    
module GraphExt(G:Graph) : Ext with type node = G.node and type graph = G.graph
                                                                 
