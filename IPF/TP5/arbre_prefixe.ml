module type Word =
  sig
    module Letter : Set.OrderedType
    type word
    val empty : word
    val is_empty : word
    val add_end : word -> Letter.t -> word
    val pop : word -> Letter.t * word
   
  end
    
module  Maketrie(W:Word) =
  struct
    type elt = W.word
    module LetterMap = Map.Make(W.Letter)
    type t = T of (bool * t LetterMap.t)
    let empty = T(false, LetterMap.t)
    let is_empty s = s = empty

    let rec cardinal s = match s with
      | T(b,m) -> let card' = LetterMap.fold (fun _ s' acc -> cardinal s'+acc) m 0 in
                  if b then 1+card' else card'
    let union s1 s2 = match s1,s2 with
      | T(b1,m1),T(b2,m2) -> let m = LetterMap.union (fun c sc1 sc2 -> Some(union sc1 sc2)) m1 m2 in
                             T(b1||b2,m)

    let taille s = if is_empty s then 0
                   else
                     

      
    let rec mem_aux w s = match s with
      | T(b,m) -> if W.is_empty w then b else
                    let (c,w') = W.pop

 (* val cardinal : 
    val nombre_ltr : word -> int
    val taille : word -> int 
    val ajout : word -> Letter.t -> word
    val union : word -> word -> word
    val terminaisons : word -> list Letter.t
    val liste_mots: word -> list Letter.t 
    *)
