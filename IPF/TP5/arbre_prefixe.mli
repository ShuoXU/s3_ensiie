module type Word =
  sig
    module Letter : Set.OrderedType
    type word
    val empty : word
    val is_empty : word
    val add_end : word -> Letter.t -> word
    val pop : word -> Letter.t * word
   (* val cardinal : 
    val nombre_ltr : word -> int
    val taille : word -> int 
    val ajout : word -> Letter.t -> word
    val union : word -> word -> word
    val terminaisons : word -> list Letter.t
    val liste_mots: word -> list Letter.t 
    *) 
  end
    
module  Maketrie(W:Word):Set with type t = W.word
