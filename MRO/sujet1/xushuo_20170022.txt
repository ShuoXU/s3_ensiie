# 1-1-1
1 1 1 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0
# 2-4-1
5 1 1 4
#3-2-1
int find_max(int *arr, int *t, int n){
	int max = t[arr[0]], buf = 0;
	int i;
	for(i = 0; i < n; i++){
		if(max < t[arr[i]]){
			max = t[arr[i]];
			buf = i;
		}
	}
	t[arr[buf]] = 0;
	return arr[buf];
}

int find_min(int *arr, int *t, int n){
	int min = t[arr[0]], buf = 0;
	int i;
	for(i = 0; i < n; i++){
		if(min > t[arr[i]]){
			min = t[arr[i]];
			buf = i;
		}
	}
	t[arr[buf]] = 1000000;
	return arr[buf];
}

void concatenation(int *SA, int *SB, int *ordering, int m, int n){
	int i;
	for (i = 0; i < m+n; i++){
		if(i < m){
			ordering[i] = SA[i];
			}
		else{
			ordering[i] = SB[i-m];
			}
	}
	for (i = 0; i < m+n; i++)
	{
		ordering[i]++; 
	}
}

void johnson_ordering_algorithm(int n, int* t1, int* t2, int* ordering){
    int i, tn, tm;
    int A[n], B[n], SB[n], SA[n];
    int a = 0, b = 0;
	for (i = 0 ; i < n ; i++){
		if (t1[i] <= t2[i]){
			A[a] = i;
			a++;
		}

		else{
			B[b] = i;
			b++;
		}
	}
	 
	for(tm = 0; tm < a; tm++){

		int p = find_min(A, t1, a);
		SA[tm] = p;
	}
	for(tn = 0; tn < b; tn++){
		int q = find_max(B, t2, b);
		SB[tn] = q;
	}
	concatenation(SA, SB, ordering, a, b);
}