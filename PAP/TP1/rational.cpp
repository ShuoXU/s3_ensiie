//============================================================================
// Name        : rational.cpp
// Author      : shuo.xu
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : TP1 pour nombres rationnels
//============================================================================

#include "rational.hpp"

namespace ensiie{

Rat::Rat(int num, int den){
  set(num,den);
};

void Rat::set_den(int den){
  if (den == 0)
    throw "Nul denominator";
  den_ = den;
}

void Rat::set(int num, int den){
  if(den == 0)
    throw "Nul denominator";
  if(num == 0)
    den = 1;
  if(den < 0)
  {
    num = -num;
    den = -den;
  }
  int d = pgcd(num, den);

  num_ = num / d;
  den_ = den / d;
}

Rat& Rat::operator+= (const Rat& r){
  set(num_ * r.den_ + den_ * r.num_, den_ * r.den_);
  return *this;
};

Rat& Rat::operator+= (int v){
  *this += Rat(v);
  return *this;
};

Rat& Rat::operator-= (const Rat& r){
  set(num_ * r.den_ - den_ * r.num_, den_ * r.den_);
  return *this;
};

Rat& Rat::operator-= (int v){
  *this -= Rat(v);
  return *this;
};

Rat& Rat::operator*= (const Rat& r){
  set(num_ * r.num_, den_ * r.den_);
  return *this;
};

Rat& Rat::operator*= (int v){
  *this *= Rat(v);
  return *this;
};

Rat& Rat::operator/= (const Rat& r){
  set(num_ * r.den_, den_ * r.num_);
  return *this;
};

Rat& Rat::operator/= (int v){
  *this /= Rat(v);
  return *this;
};

Rat operator+  (const Rat& r1, const Rat& r2){
  Rat r = r1;
  return r += r2;
};

Rat operator-  (const Rat& r1, const Rat& r2){
  Rat r = r1;
  return r -= r2;
};

Rat operator*  (const Rat& r1, const Rat& r2){
  Rat r = r1;
  return r *= r2;
};

Rat operator/  (const Rat& r1, const Rat& r2){
  Rat r = r1;
  return r /= r2;
};

bool operator==(const Rat & r1, const Rat & r2){
  return r1.num_ * r2.den_ == r1.den_ * r2.num_;
};

bool operator==(const Rat & r1, int v){
  return r == Rat(v);
};

bool operator!=(const Rat & r1, const Rat & r2){
  return !(r1 = r2);
};

bool operator<(const Rat & r1, const Rat & r2){
  return r1.num_ * r2.den_ < r1.den_ * r2.num_;
};

bool operator<=(const Rat & r1, const Rat & r2){
  return r1.num_ * r2.den_ <= r1.den_ * r2.num_;
};

bool operator>(const Rat & r1, const Rat & r2){
  return r1.num_ * r2.den_ > r1.den_ * r2.num_;
};

bool operator>=(const Rat & r1, const Rat & r2){
  return r1.num_ * r2.den_ >= r1.den_ * r2.num_;
};

std::ostream & operator<< (std:ostream st, const Rat& r){
  st << r.num_;
  if(r.den_ != 1)
    st<< "/" << r.den_;
  return st;
};

int Rat::pgcd(int a, int b){
  if (a == 0) return 1;
  if (a < 0) a = -a;
  if (b < 0) b = -b;
  if (a < b) {
    int tmp = a;
    a = b;
    b = tmp;
  }
  int r =  a;
  while(r != 0){
    r = a % b;
    a = b;
    b = r; 
  }
  return a;
}
}//namespace ensiie