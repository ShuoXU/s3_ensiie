//============================================================================
// Name        : rational.hpp
// Author      : shuo.xu
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : TP1 pour nombres rationnels
//============================================================================
#ifndef ENSIIE_RATIONAL_H
#define ENSIIE_RATIONAL_H
#include <ostream>
namespace ensiie{

class Rat
{
 private://data numbers
  int num_;
  int den_;

 public://structors
  Rat(int num, int den = 1);
  
 public://getters
  int get_num() const {return num_;};
  int get_den() const {return den_;};

 public://setters
  void set_num(int num){num_ = num;};
  void set_den(int den);
  void set(){int num,den;};

 public://operators 
  Rat & operator+= (const Rat & r);
  Rat & operator-= (const Rat & r);
  Rat & operator*= (const Rat & r);
  Rat & operator/= (const Rat & r);

  Rat & operator+= (int v);
  Rat & operator-= (int v);
  Rat & operator*= (int v);
  Rat & operator/= (int v);

  friend Rat operator+  (const Rat& r1, const Rat & r2);
  friend Rat operator-  (const Rat& r1, const Rat & r2);
  friend Rat operator*  (const Rat& r1, const Rat & r2);
  friend Rat operator/  (const Rat& r1, const Rat & r2);

  friend bool operator==  (const Rat& r1, const Rat & r2);
  friend bool operator==  (const Rat& r1, int v);
  friend bool operator!=  (const Rat& r1, const Rat & r2);
  friend bool operator>=  (const Rat& r1, const Rat & r2);
  friend bool operator<=  (const Rat& r1, const Rat & r2);
  friend bool operator<   (const Rat& r1, const Rat & r2);
  friend bool operator>   (const Rat& r1, const Rat & r2);

  friend std::ostream & operator<<(std::ostream & st, const Rat & r);

 private:
 int pgcd(int a, int b);
 };  

};//namespace ensiie

#endif //ENSIIE_RATIONAL_H
