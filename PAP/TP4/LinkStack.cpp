#include <iostream>
#include <stdlib.h>
using namespace std;

#define MAXSIZE 0xffff

template<class T>
struct LinkNode{
  T data;
  LinkNode *link;
  LinkNode(const T& args, LinkNode<T> *ptr = NULL){
    data = args;
    link = ptr;
  }
};  


template<class T>
class LinkStack
{
  LinkNode<T> *top_;

public :

  LinkStack()
  {
    top_ = NULL;
  }

  LinkStack(int size)
  {
    top_ = NULL;
  }

  ~LinkStack()
  {
    empty();
  }

  // Stack(Stack &s)
  //{
  //}

  bool is_empty();
  void empty();
  void push(const T& item);
  T pop();
  T top();

};

template<class T>
bool LinkStack<T>::is_empty(){
  if (top_==NULL){
    return true;
  }
    else {
      return false;
    }
}

template<class T>
void LinkStack<T>::empty(){
  LinkNode<T> *p;
    while(top_!=NULL)
      {
	p = top_;
	top_ = top_->link;
	delete p;
      }
}

template<class T>
void LinkStack<T>::push(const T& item){
  top_ = new LinkNode<T> (item, top_);
}

template<class T>
T LinkStack<T>::pop(){
  if (top_!=NULL){
    T tmp = top_->data;
    LinkNode<T> *p = top_;
    top_ = top_->link;
    delete p;
    return tmp;
  }
  else {
    cout << "We can't pop from empty~~~";
  }
}

template<class T>
T LinkStack<T>::top(){
  if(top_ != NULL) {
    return top_->data;
    }
  else {
    cout << "No top from empty~~~";
  }
}


