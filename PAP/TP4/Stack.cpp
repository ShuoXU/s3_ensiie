#include <iostream>
#include <stdlib.h>
using namespace std;

#define MAXSIZE 0xffff

template<class T>

class Stack
{
  int max_size_;
  int top_;
  T* array_;

public :

  Stack():top_(-1), max_size_(MAXSIZE)
  {
    array_ = new T[max_size_];
    if(array_ == NULL)
      {
	cerr << "distribution failed~~~" << endl;
	exit(1);
      }
  }

  Stack(int size):top_(-1), max_size_(MAXSIZE)
  {
    array_ = new T[max_size_];
    if(array_ == NULL)
      {
	cerr << "distribution failed~~~" << endl;
	exit(1);
      }
  }

  ~Stack()
  {
    delete[] array_;
  }

  // Stack(Stack &s)
  //{
  //}

  bool is_empty();
  void empty();
  void push(const T& item);
  T pop();
  T top();

};

  template<class T>
  bool Stack<T>::is_empty(){
    if (top_==-1){
      return true;
    }
    else {
      return false;
    }
  }

  template<class T>
  void Stack<T>::empty(){
    top_ = -1;
  }

  template<class T>
  void Stack<T>::push(const T& item){
    if(top_ + 1 < max_size_){
      array_[++top_] = item;
    }
    else{
      cout << "The stack is full~~~";
      exit(1);
    }
  }

  template<class T>
  T Stack<T>::pop(){
    if (top_>=0){
      top_--;
      return array_[top_+1];
    }
    else {
      cout << "We can't pop from empty~~~";
    }
  }

  template<class T>
  T Stack<T>::top(){
    if(top_ != -1) {
      return array_[top_];
    }
    else {
      cout << "No top from empty~~~";
    }
  }


