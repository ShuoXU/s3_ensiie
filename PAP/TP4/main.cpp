#include <iostream>
#include "LinkStack.cpp"

int main(){
  LinkStack<int> stk;
  int i;
  for (i = 0; i < 20; i++){
    stk.push(i);
  }
  cout<<"My top is "<<stk.top()<<endl;
  cout<<"My first pop is "<<stk.pop()<<endl;
  cout<<"Now the top is "<<stk.top()<<endl;

  return 0;
}
