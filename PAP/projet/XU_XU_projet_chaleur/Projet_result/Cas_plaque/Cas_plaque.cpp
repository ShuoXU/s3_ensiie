/**
 * @file Cas_barre.cpp
 * @brief its for the first situation
 * @author XU Jiahui
 * @email jiahuixuu@gmail.com
 * @date 2019-1-10
 */
# include <cstdlib>
# include <iostream>
# include <iomanip>
# include <fstream>
# include <cmath>

using namespace std;
/**
 * @brief This class is just for keeping all the material
 * It has three public member variables and a Constructor to set all the variables
 */
class material{
public:
    double lambda;///< Thermal Conductivity
    double rho;///< density
    double c;///< Specific heat capacity
    /**
    * @brief Constructor
    */
    material(double _lambda, double _rho, double _c){
        lambda = _lambda;
        rho = _rho;
        c = _c;
    }
};
/**
 * @brief This class is for keeping the distance, we can also consider it as keeping all the points
 * It has five public member variables and a Constructor to set all the variables, i give it 1001 points as default
 */
class X{
public:
    double x_left;///< left border
    double x_right;///< right border
    int numb;///< number of points included 
    double delta_x;///< Point spacing
    double *x;///< table contains all the points
    /**
    * @brief Constructor
    */
    X(){
        x_left = 0.0;
        x_right = 1.0;
        numb = 1001;
        delta_x = (x_right - x_left) / (double) (numb - 1);
        x = new double[numb];
    }
};
/**
 * @brief This class is for keeping the time
 * It has five public member variables and a Constructor to set all the variables
 */
class T{
public:
    double t_left;///< left border
    double t_right;///< right border
    int numb;///< number of time points included 
    double delta_t;///< time interval
    double *t;///< table contains all the time points
    /**
    * @brief Constructor
    */
    T(){
        t_left = 0.0;
        t_right = 16.0;
        numb = 100;
        delta_t = (t_right - t_left) / (double) (numb - 1);
        t = new double[numb];
    }
};
/**
 * @brief this function is to set the initial temperature
 *
 * @param n the size of the table u
 * @param u a table of double
 */
void Set_u0(int n, double u[]){
    for(int i = 0; i < n; i++) {
        u[i] = 250.15;
    }
}

void Set_u0(int n, double **u) {
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++) {
            u[i][j] = 250.15;
        }
    }
}
/**
 * @brief this function is to set the matrix for the eurle methode
 *
 * @param n the size of the table 
 * @param a matrix without initialization
 * @param w a constant determined by the physical properties of the object
 * @return there is no return value
 *   @retval because the table a is a pointer
 */
void Set_a(int n, double a[], double w){
    a[0+0*3] = 0.0;
    a[1+0*3] = 1.0;
    a[0+1*3] = 0.0;

    for(int i = 1; i < n - 1; i++ )
    {
        a[2 + (i - 1) * 3] =           - w;
        a[1 + i  * 3]      = 1.0 + 2.0 * w;
        a[0 + (i + 1) * 3] =           - w;
    }

    a[2 +( n - 2) * 3] = 0.0;
    a[1 +( n - 1) * 3] = 1.0;
    a[2 +(n - 1) * 3] = 0.0;
}
/**
 * @brief this function is to set the matrix b
 *
 * @param n the size of the table 
 * @param index shows the time 
 * @param delta_t time interval
 * @param u and b is a table
 * @param F shows the temperature source
 * @param obj shows which kind of material we are working on
 * @return there is no return value
 *   @retval because the table b is a pointer
 */
void Set_b(int n, int index, double delta_t, double *u, double *b, double *F, material *obj) { 
    b[0] = 250.15;
    for(int i = 1; i < n -1; i++){
        b[i] = u[i + (index - 1) * n] + delta_t * F[i] / (obj->rho * obj->c);
    }
    b[n-1] = 250.15;
}
/**
 * @brief this function is to set the temperature source
 *
 * @param n the size of the table 
 * @param F shows the temperature source
 * @param coef these four coef show us the locaiton of the source
 * @return there is no return value
 *   @retval because the table F is a pointer
 */
void Set_F(int n, double *F, double coef1, double coef2, double coef3, double coef4){
    for (int i = n*coef1; i <= n*coef2; i++){
        F[i] = 16*(80+237.15)*(80+237.15);
    }
    for (int i = n*coef3; i < coef4*n; i++){
        F[i] = 12*(80+237.15)*(80+237.15);
    }
}
/**
 * @brief this function is to transform the matrix a to a triangular matrix
 *
 * @param n the size of the table 
 * @param a target matrix
 * @return there is no return value
 *   @retval because the table F is a pointer
 */
void Decomposition (int n, double a[])
{
    for(int i = 1; i <= n-1; i++ )
    {
        a[2+(i-1)*3] = a[2+(i-1)*3] / a[1+(i-1)*3];
        a[1+i*3] = a[1+i*3] - a[2+(i-1)*3] * a[0+i*3];
    }
}
/**
 * @brief this function is to solve the problem with LU methode
 *
 * @param n the size of the table 
 * @param a coef matrix
 * @param b value vector
 * @return returns the resolution 
 *   @retval its a vector
 */
double *Resolution (int n, double a[], double b[]) {
  int i;
  double *x;

  x = new double[n];

  for ( i = 0; i < n; i++ )
  {
    x[i] = b[i];
  }

    for ( i = 1; i < n; i++ )
    {
      x[i] = x[i] - a[2+(i-1)*3] * x[i-1];
    }

    for ( i = n; 1 <= i; i-- )
    {
      x[i-1] = x[i-1] / a[1+(i-1)*3];
      if ( 1 < i )
      {
        x[i-2] = x[i-2] - a[0+(i-1)*3] * x[i-1];
      }
    }

    return x;
}
/**
 * @brief this function is to write the result into a file
 *
 * @param n,m show us the one we are writing in the table
 * @param table contains all the things we want to write
 * @return no returns
 */
void dtable_data_write ( ofstream &output, int m,double **table){
  int i;
  int j;

  for ( j = 0; j < m; j++ )
  {
    for ( i = 0; i < m; i++ )
    {
      output << table[j][i] << "\t";
    }
    output << "\n";
  }

  return;
}

void dtable_write ( string output_filename, int m,double **table){
  ofstream output;

  output.open ( output_filename.c_str ( ) );

  if ( !output )
  {
    cerr << "\n";
    cerr << "DTABLE_WRITE - Fatal error!\n";
    cerr << "  Could not open the output file.\n";
    return;
  }

  dtable_data_write ( output, m, table );

  output.close ( );

  return;
}
/**
 * @brief where we use all the function, here we instantiated the objs of the materials, 
 *          and for each of them we use euler methode to simulate the temperature curve
 */
int main(){
    material *objs[4];
    objs[0] = new material(389.0, 8940.0, 380.0);   //cuivre
    objs[1] = new material(80.2, 7874.0, 440.0);    //fer
    objs[2] = new material(1.2, 2530.0, 840.0);     //verre
    objs[3] = new material(0.1, 1040.0, 1200.0);    //polystyrene   

    X x;
    T t;

    string u_file;

    double *result = new double[x.numb];
    double *u = new double[x.numb * t.numb];
    Set_u0(x.numb, u);

    double *a = new double[3 * x.numb];

    double **res = (double**)malloc(sizeof(double*)*x.numb);
    for(int i = 0; i < x.numb; i++){
        *(res+i) = (double*)malloc(sizeof(double)*x.numb);//开辟列
    }
    Set_u0(x.numb, res);

    for(int obj_num = 0; obj_num < 4; obj_num++){
        double k = objs[obj_num]->lambda/(objs[obj_num]->rho * objs[obj_num]->c);
        double w = k * t.delta_t / x.delta_x / x.delta_x;

        Set_a(x.numb, a, w);
        Decomposition(x.numb, a);
        
        double *b = new double[x.numb];
        double *F = new double[x.numb];

        for(int i  = 1; i < t.numb; i++) {
            Set_F(x.numb, F, 0.16, 0.32, 0.66, 0.83);
            Set_b(x.numb, i, t.delta_t, u, b, F, objs[obj_num]);
            delete [] F;

            F = Resolution(x.numb, a, b);
            for (int j = 0; j < x.numb; j++ ){
                u[j+i*x.numb] = F[j];
            }
            for (int j = 0; j < x.numb; j++ ){
                result[j] = F[j];
            }
        }
        for(int raidus = 249; raidus >= 0; raidus--){
            for(int m = 1; m < x.numb; m++) {
                for(int n = 1; n < x.numb; n++) {
                    if( (int)(sqrt((m-250)*(m-250)+(n-250)*(n-250))) == raidus ||
                        (int)(sqrt((m-250)*(m-250)+(n-750)*(n-750))) == raidus ||
                        (int)(sqrt((m-750)*(m-750)+(n-750)*(n-750))) == raidus ||
                        (int)(sqrt((m-750)*(m-750)+(n-250)*(n-250))) == raidus)
                        res[m][n] = F[250-raidus];
                }
            }
        }

        u_file = to_string(obj_num) + "u.txt";
        dtable_write ( u_file, x.numb,res);
    }
    delete [] result;
    delete [] u;
    delete [] a;
    return 0;
}
