/*@ ensures  x> y ==> \result == x;
  ensures  x <= y ==> \result == y;
*/
int maxint (int x, int y)
{
  return (x > y) ? x : y;
}
